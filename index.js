// Seu código principal (index.js)

// Adiciona o módulo do New Relic
require('newrelic');

const express = require('express');
const app = express();
app.use(express.json()); // Para suportar corpos de requisição JSON

app.post('/webhook', (req, res) => {
  console.log('Recebido webhook!');
  console.log(req.body); // Exibir o corpo da requisição no console
  res.status(200).send('Webhook recebido com sucesso!');
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Servidor rodando na porta ${PORT}`);
});
